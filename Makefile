
#
# General purpose Makefile for C.
# 
# The Makefile is licensed under MIT
# and is part of the Pinit project.
#
# Author: Preben Vangberg
# Since: 21.10.2018 
#

OS = Linux

VERSION = 1.1.0
CC	= gcc

TARGET := bin/main
PARAM := -Wall

SRCDIR := src
OBJDIR := obj

SRCS := $(shell find $(SRCDIR) -name "*.c")
OBJS := $(SRCS:$(SRCDIR)/%.c=$(OBJDIR)/%.o)

$(TARGET): $(OBJS)
	@mkdir -p bin
	$(CC) -o $@ $^

$(OBJDIR)/%.o: $(SRCDIR)/%.c
	@mkdir -p obj
	$(CC) $(PARAM) $(@:$(OBJDIR)/%.o) -o $@ -c $<

clean: 
	rm -f $(OBJDIR)/*.o
	
all: clean $(TARGET)

#include<stdio.h>
#include<stdlib.h>
#include<time.h>

int check_x_axis(char board[9][9][9], int number, int x, int y);
int check_y_axis(char board[9][9][9], int number, int x, int y);
int check_3x3_tile(char board[9][9][9], int number, int x, int y);
int find_position(int position);
void print_table(char board[9][9][9]);
void check_complete(char board[9][9][9]);
	
int main(){	
	//To se how fast program is
	clock_t start, end;
	double cpu_time_used;

	char board[9][9][9]; //Board used for sudoku

	FILE *fp; //File Pointer, used to read files
	char filename[30]; //Name of file to read the board from


	//-----SET-UP-READ-----
	printf("Enter the name of the board text file: ");
	//strcpy(filename,"board1.txt");
	scanf("%s", filename);
	
	fp = fopen(filename, "r");
	if(!fp){
		printf("ERROR, could not find file\n");
		exit(EXIT_FAILURE);
	}
	start = clock();
	printf("File Found!\n");	
	//-----READ-FROM-FILE-----
	for(int y = 0; y < 9; y++){
		for(int x = 0;  x < 9; x++){
			int p = fscanf(fp, "%hhd", &board[x][y][0]);
			if(p != 1){
				printf("ERROR, I read to much to handle");
				exit(EXIT_FAILURE);
			}
			//Puts every value in the z-axis to 0
			for(int z = 1; z < 9; z++){
				board[x][y][z] = 0;
			}
		}
	}

	//-----PRINT-OUT-BOARD-----
	
	print_table(board);

	//-----SOLVING-LOOP-----	
	for(int y = 0; y < 9; y++){
		for(int x = 0; x < 9; x++){
			if(board[x][y][0] == 0){ //checks if the tile is empty
				for(int zero_reset = 1; zero_reset < 9; zero_reset++){
					board[x][y][zero_reset] = 0;
				}
				//Checks if x line has this a value			
				//Current value being checked 
				for(int number = 1; number < 10; number++){ 
					//Checks if axis and tile don't have the number
					if((check_x_axis(board, number, x, y) 
							+ check_y_axis(board, number, x, y)
						   	+ check_3x3_tile(board, number, x, y)) == 3){
						int null_check = 1;
						//Searches for empty place to store number
						while(board[x][y][null_check] != 0){
							null_check++;
						}
						board[x][y][null_check] = number;
					}
				}
				//If only one value is availible then put that one  
				//onto the board
				if(board[x][y][2] == 0 && board[x][y][1] != 0){
					board[x][y][0] = board[x][y][1];
					board[x][y][1] = '0';
					printf("Changed made on %d and %d to the number %d\n",x,y,board[x][y][0]);
					x = -1; //resets the check of the board
					y = 0;
				}
			}		
		}
	}
	check_complete(board);
	print_table(board);
	end = clock();
	cpu_time_used = ((double)(end - start)) / CLOCKS_PER_SEC;
	printf("\nTime of program: %fs\n",cpu_time_used);
	return 0;
}

/**
 * check the y acxis of an 3d array 
 * if the number given is on the y axis
 * then return 0
 * if not return 1
 */
int check_y_axis(char board[9][9][9], int number, int x, int y){
	for(int yc = 0; yc < 9; yc++){
		//If number is in line, start over with new number
		if(board[x][yc][0] == number){ 
			return 0;
		}
	}
	return 1;
}

/**
 * Check the x axis of an 3d array
 * if the number given is on the x axis
 * then return 0
 * if not return 1
 **/
int check_x_axis(char board[9][9][9], int number, int x, int y){
	for(int xc = 0; xc < 9; xc++){ 
		if(board[xc][y][0] == number){
		return 0;
		}
	}
	return 1;
}
/**
 *Checks if there the number is in the 3x3 tile
 *
 */
int check_3x3_tile(char board[9][9][9], int number, int x, int y){
	//Finds the top left of the 3x3 tile of this value
	int y_top = find_position(y);
	int x_left = find_position(x);
	for(int yc = y_top; yc < (y_top + 3); yc++){
		for(int xc = x_left; xc < (x_left + 3); xc++){
			if(board[xc][yc][0] == number){
				return 0;
			}
		}
	}
	return 1;
}

/*
 *Finds the position of the position of top/left of
 *the tile
 */
int find_position(int position){
	if((position % 3) == 0 ){	//If on top of tile
		return position;
	}
	else if(((position + 1) % 3) == 0){	//If on bottom of tile
		return position - 2;
	}
	else if(((position + 2) % 3) == 0){	//If in middle of tile
		return position - 1;
	}
	return 0;
}

/*
 *Printing out the table
 */
void print_table(char board[9][9][9]){
	printf("------------------------\n");
	for(int y = 0; y < 9; y++){
		printf("| ");
		for(int x = 0; x < 9; x++){
			printf("%d ", board[x][y][0]);
			if((x + 1) % 3 == 0) printf("| ");
		}
		printf("\n");
		if((y + 1) % 3 == 0){ printf("------------------------\n");}
	}
} 
/*
 * Checks if table is complete
 */
void check_complete(char board[9][9][9]){
	for(int y = 0; y < 9; y++){
		for(int x = 0; x < 9; x++){
			if(board[x][y][0] == 0){
				printf("\nFailed to complete board\n");
				printf("First '0' was at = x:%d ,  y:%d\n", x, y);
				return;
			}
		}
	}
	printf("Board successfully completed\n");
}
